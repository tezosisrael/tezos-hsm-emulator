#!/bin/bash
set -euo pipefail
# IFS=$'\n\t'

client=${1:-"tezos-client"}
baker_secret_key=${2:-}
which tezos-client

if [[ "$baker_secret_key" == "" ]]
then
    echo "missing baker_secret_key arg"
    echo "usage: ./import-baker.sh client_cmd baker_secret_key"
    exit 1
fi

runclient () {
    cmd="$client ${1:-}"
    echo ""
    echo "------ $cmd"
    $cmd
    ret=$?
    echo ""
    echo "------"
    echo ""
    return $ret
}

bake_for () {
    runclient "bake for $1 --minimal-timestamp"
}

runclient "import secret key baker ${baker_secret_key}" || true

runclient "transfer 2000000 from bootstrap2 to baker --burn-cap 0.257 " & (sleep 1 && bake_for "bootstrap2")
runclient "transfer 2000000 from bootstrap1 to baker --burn-cap 0.257 " & (sleep 1 && bake_for "bootstrap2")


runclient "register key baker as delegate"

for i in $(seq 1000); do
    set +e
    bake_for baker
    ret="$?"
    set -e

    if [[ "$ret" != "0" ]]
    then
        echo "Baker cannot bake at $i ($?)"
        bake_for bootstrap3
    else
        echo "Done at $i : $ret"
        exit 0
    fi
done
