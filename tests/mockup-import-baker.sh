#!/bin/bash
set -euo pipefail
# IFS=$'\n\t'

# export TEZOS_LOG='* -> debug'
echo "mockup" $SHLVL
# set -ex
mockup_dir="mckp"
chain="NetXynUjJNZm7wi"
baker_secret_key=${1:-"unencrypted:edsk31vznjHSSpGExDMHYASz45VZqXN4DPxvsa4hAyY8dHM28cZzp6"}
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

client="$TEZOS_CLIENT --mode mockup --base-dir $mockup_dir --protocol ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK"

rm -Rf $mockup_dir


if [[ "$TEZOS_CLIENT" == "" ]]
then
    echo "TEZOS_CLIENT env should be set with the the tezos-client"
    exit 1
fi

eval $client "create mockup --asynchronous"

${__dir}/import-baker.sh "$client" "$baker_secret_key"

echo "To use the same mockup client run:"
echo $client