# Tests

- test signing each operation type:
  - sign block (bake)
    - `tezos-client bake for baker`
  - operations:
    - reveal
      - `tezos-client reveal key for baker`
    - endorsing
      - `tezos-client endorse for baker`
    - seed_nonce_revelation
      - `tezos-client reveal nonces`
    - double_endorsement_evidence
    - double_baking_evidence
    - proposals
      - `tezos-client submit proposals for baker proposals...`
    - ballot
      - `tezos-client submit ballot for baker <proposal> <ballot:yay/nay/pass>`
    - transaction
      - `tezos-client transfer X from baker to Y --burn-cap 0.257`
    - origination
      - `tezos-client originate contract CONTRACT_NAME transferring <qty> from baker running <prg>`
    - delegation
      - `tezos-client set delegate for <src> to <mgr>`
- need to validate that signing is correct

- test running a bakery on testnet (local and public networks)
- test running a real bakery

## Setup

start by building tezos from source: http://tezos.gitlab.io/introduction/howtoget.html#set-up-the-development-environment

## Mockup test setup

this test relies on branch `richard.bonichon/tezos-mockup-fake-baking` of tezos

- use `./tests/mockup-import-baker.sh <baker_secret_key>` to create a baker:

for importing from signer: `./tests/mockup-import-baker.sh http://signerip:signerport/tz1_xxxxxxxxxxx` where tz1_xxxxxxxxxxx is the address

## Sandboxed test setup

run the following commands in separate terminals in the tezos path:

```bash
./src/bin_node/tezos-sandboxed-node.sh 1 --connections 1
```

```bash
eval `./src/bin_client/tezos-init-sandboxed-client.sh 1`
tezos-autocomplete
tezos-activate-alpha
```

after the second command tezos-client is using the Sandboxed env. it will use the node running in the first command.

to create a baker on the created network run (similar command to mockup):

```bash
./tests/import-baker.sh "tezos-client" "$private_key"
```
