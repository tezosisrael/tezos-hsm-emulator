basically our wallet will need to encode the transactions and then push them to a node

create keyreveal

links for reference

https://github.com/obsidiansystems/ledger-app-tezos/tree/master/src   http://tezos.gitlab.io/introduction/howtoget.html

https://github.com/AndrewKishino/sotez

https://github.com/tezbridge/tezbridge-crypto - this library pretty much does everything we need, we just need to use it (or simplify the operations) and wrap it with a module that knows how to speak to the node, and a cli

https://medium.com/tqtezos/lifecycle-of-an-operation-in-tezos-248c51038ec2

https://tezos.stackexchange.com/questions/76/how-to-make-an-offline-transaction/89#89

accounts: tz1: ed25519 tz2: Secp256k1 (bitcoin) tz3: NIST p256r1 curve, secp256r1

questions:

1. what is preapply operation?
2. what is the lifecycle of an operation? which parts of the lifecycle can be done offline?
3. are all operations on tezos done the same? do they all have the same lifecycle?
4. which types of operations exist?
5.

6. is nonce create random?
