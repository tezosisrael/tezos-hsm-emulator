## Background:

Trying to add tezos support to an existing HSM. The requirement is that all secured operations happen offline inside the HSM.

## Plan

### First step:

Implement a mockup HSM in JS, that will have an API to talk with the different tezos-\* CLIs. Need to plan the architecture for this HSM and CLIs.

### Second step:

Implement the API inside the HSM and replace the component in the above architecture.

## Existing way to run a bakery

### 1. Generate keys

```
tezos-client gen keys baker
```

Baker is the alias for the generated keys.

### 1a. Importing keys from HSM

```
tezos-client import public key <new> <uri>
```

### 2. Reveal key on the blockchain

This is usually done when funds are transferred to the account

```
tezos-client reveal key for baker
```

### 3. Register bakery

```
tezos-client register key baker as delegate
```

can check baker's rights here:

```
tezos-client rpc get /chains/main/blocks/head/helpers/baking_rights\?cycle=300\&delegate=tz1_xxxxxxxxxxx\&max_priority=2
```

### 4. Run services:

#### Baker:

The baker is a daemon that once connected to an account, computes the baking rights for that account, collects transactions from the mempool, and bakes a block. Note that the baker is the only program that needs direct access to the node data directory for performance reasons.

```bash
tezos-baker-PROTO run with local node ~/.tezos-node bob
```

#### Endorser

The endorser is a daemon that once connected to an account, computes the endorsing rights for that account and, upon reception of a new block, verifies the validity of the block and emits an endorsement operation. It can endorse for a specific account or if omitted it endorses for all accounts.

```bash
tezos-endorser-PROTO run
```

### Accuser

The accuser is a daemon that monitors all blocks received on all chains and looks for:

- bakers who signed two blocks at the same level
- endorsers who injected more than one endorsement operation for the same baking slot

Upon finding such irregularity, it will emit respectively a double-baking or double-endorsing denunciation operation, which will cause the offender to lose its security deposit.

```bash
tezos-accuser-PROTO run
```

## Operations:

- Endorsement
- Seed_nonce_revelation
- Double_endorsement_evidence
- Double_baking_evidence
- Activate_account
- Proposals
- Ballot
- Reveal
- Transaction
- Origination
- Delegation

Baking blocks doesn't count as an operation, as it basically the signing of a block of transactions.

## Baking

Will be investigated

## Resources to use:

- Tezos Docs
  - Running a bakery http://tezos.gitlab.io/introduction/howtorun.html
  - PoS http://tezos.gitlab.io/whitedoc/proof_of_stake.html
- Libraries
  - Sotez - https://github.com/AndrewKishino/sotez
  - Tezbridge https://github.com/tezbridge/tezbridge-crypto
- Code
  - Ledger Nano Tezos apps - https://github.com/obsidiansystems/ledger-app-tezos/tree/master/src
- Articles:
  - Lifecycle of an operation - https://medium.com/tqtezos/lifecycle-of-an-operation-in-tezos-248c51038ec2

## Questions that needs investigation

- How is the seed nonce created? is it randomly selected?
- What inputs do we need to sign a block?
- How do we inject a block to the blockchain after signing? Baker Service -> RPC -> Node
- Do we really need to implement tezos-{baker,accuser,endorser}? or does creating a client which knows how to interact with the HSM, and how to talk to them is enough?
