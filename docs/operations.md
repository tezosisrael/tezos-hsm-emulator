## Operations on the blockchain

Every operation has the parameter kind which is usually a hardcoded value

### Accounts

#### Reveal

reveal the public key on the blockchain.

From the docs

```
The client does a bit of magic to simplify our life and here we see that many details were automatically set for us. Surprisingly, our transfer operation resulted in two operations, first a revelation and then a transfer. Alice’s address, obtained from the faucet, is already present on the blockchain, but only in the form of a public key hash tz1Rj...5w. In order to sign operations Alice needs to first reveal the public key edpkuk...3X behind the hash, so that other users can verify her signatures. The client is kind enough to prepend a reveal operation before the first transfer of a new address, this has to be done only once, future transfers will consist of a single operation as expected.
```

#### Activate_account

(fund raiser)

### Endorsing

#### Endorsement

Params:

- R kind: `endorsement`
- R level: integer, the block level

### Baking

#### Seed_nonce_revelation

https://tezos.gitlab.io/whitedoc/proof_of_stake.html#random-seed

The random seed is used to define

Params:

- R kind: `seed_nonce_revelation`
- R level: integer, the block level
- R nonce: string, `^[a-zA-Z0-9]+$`

### Accusing

#### Double_endorsement_evidence

#### Double_baking_evidence

### Proposals

#### Proposals

#### Ballot

### Wallet

#### Transaction

#### Origination

#### Delegation
