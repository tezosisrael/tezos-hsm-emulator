# Tezos HSM emulator

As a wallet on the tezos blockchain we want to give the following abilities:

- Setup a new account
- Restore an account from mnemonic
- Sign operations

## Using this repository

This repository is created from three different packages:

- siglib - Signer Library
- server - The signer server
- starter - a small cli that creates mnemonic and generates the key pair and its address on the tezos blockchain.

### Install dependencies

run

```bash
yarn
```

### Setup an account

run

```bash
yarn setup
```

This will create an account and run the signer

### Run the server

On the next usages, running the following command will run a server without creating an account:

```bash
yarn start
```

## Architecture

![Architecture](./hsm-tezos-diagram.png)

- Tezos - this is the tezos binaries, they provide the standard API for our users to talk and sign operations using the HSM. Nothing _should_ be changed here.
- Signer API - provides a lightweight server that exposes the standard API for tezos-client, it uses the Signer Lib to communicate with the HSM.
- Signer Lib - library that connects between the HSM to the signer API
- HSM Service - The service that signs operations and creates accounts.

## Signer API

exposes the following API:

- `GET /authorized_keys` Retrieve the public keys that can be used to authenticate signing commands. If the empty object is returned, the signer has been set to accept unsigned commands. (most signers I saw just return empty object) returns {authorized_keys: [pkh]}

- `GET /keys/:pkh` Retrieve the public key of a given remote key (=public key hash) returns {public_key: pk}

- `POST /keys/:pkh` Sign a piece of data with a given remote key receives: a string of bytes returns {signature: Signature.encoding}

## SignerLib

- getPublicKey(pkh: string): string returns the public key which corresponds to the provided public key hash (pkh)
- sign(pkh: string, bytes: string): string signs the provided bytes with the secret key of pkh. throws an error if can't sign
- getAuthorizedKeys() : string[] Retrieve the public keys that can be used to authenticate signing commands.

## HSMService

a cli service that provides a user interface to generate keys and sign operations and an api for the SignerLib to connect with.

When starting the service for the first time the user sees the following menu:

- Restore account from seed - on choosing this the user will be able to write his mnemonic seed and choose his pin
- Create seed - creates a new random mnemonic seed

(to keep data between startups we will use a text file)

On further startups and after seed was restored/created the user will see the following:

- Show mnemonic
- Baking App
- Wallet App
- Set high watermark

The apps are separated to support some security measure. A user can bake only when the baking app is activated, and only with the account he chooses. No other operations are available, and every operation is signed automatically without approval. This is to maintain 100% availability, and to prevent the user from the need to stay by the wallet and approve operations.

When the wallet app is activated, baking operations are not permitted, and each operation needs to be approved by the user.

The high watermark is used to prevent double baking/endorsements. One of the checks before signing the operation would be to check if the operation block level is higher than the watermark, if not it will fail. If the operation is signed and it's a baking operation (baking, endorsement), the watermark will be set to the current level.

### Baking app

Asks for PIN on the start, and then asks the user to choose an account to bake with. Needs to stay open as long as the user wants to bake. Doesn't ask for PIN afterwards

When active can only sign the following:

- Baking
- Endorsement
- Nonce revelation
- Double baking/endorsement revelation.

### Wallet app

Asks for PIN on the start, and asks for it again + confirmation for any operation that the HSMService is required to sign. Can sign any operation that is not in the list above.
