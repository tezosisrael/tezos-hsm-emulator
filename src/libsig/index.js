const bip39 = require('bip39');

const ed25519 = require('./ed25519');
const p256 = require('./p256');
const secp256k1 = require('./secp256k1');

const { isBlock, isEndorsement } = require('./operation');

const schemes = { ed25519, p256, secp256k1 };

module.exports = {
  generateMnemonic,
  generateKeys,
  sign,
  verify,
  isBlock,
  isEndorsement,
};

function generateMnemonic() {
  return bip39.generateMnemonic(160);
}

function generateKeys(mnemonic, password, scheme = 'ed25519') {
  return schemes[scheme].generateKeys(mnemonic, password);
}

function sign(bytes, sk, scheme = 'ed25519') {
  return schemes[scheme].sign(bytes, sk);
}

function verify(bytes, signature, pk, scheme = 'ed25519') {
  return schemes[scheme].verify(bytes, signature, pk);
}
