const path = require('path');
const fs = require('fs').promises;

const { isExists } = require('../file');

const { getMagicBytes, blockLevel } = require('./operation');
const WM_FILE = 'watermarks.json';

async function getWatermarks(dir) {
  let file = path.join(dir, WM_FILE);
  if (!(await isExists(file))) {
    return {};
  }

  let json = (await fs.readFile(file)).toString();
  return json.length === 0 ? {} : JSON.parse(json);
}

async function canSign(dir, pkh, bytes) {
  const watermarks = await getWatermarks(dir);
  const magicBytes = getMagicBytes(bytes);

  const watermark = watermarks[`${pkh}_${magicBytes}`];
  const currentBlockLevel = blockLevel(bytes);
  const watermarkBlockLevel = watermark && blockLevel(watermark.bytes);

  return !watermark || currentBlockLevel > watermarkBlockLevel;
}

async function setWatermark(dir, pkh, bytes) {
  const file = path.join(dir, WM_FILE);
  const watermarks = await getWatermarks(dir);
  const magicBytes = getMagicBytes(bytes);
  watermarks[`${pkh}_${magicBytes}`] = {
    pkh,
    bytes,
  };

  await fs.writeFile(file, JSON.stringify(watermarks, null, 2));
}

module.exports = {
  canSign,
  setWatermark,
};
