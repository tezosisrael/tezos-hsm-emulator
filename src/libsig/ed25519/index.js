const _sodium = require('libsodium-wrappers');
const bip39 = require('bip39');

const {
  b58cencode,
  b58cdecode,
  hex2buf,
  mergebuf,
  buf2hex,
} = require('./utils');
const prefix = require('./prefix');

module.exports = {
  generateKeys,
  sign,
  verify,
};

async function generateKeys(mnemonic, password) {
  await _sodium.ready;
  const sodium = _sodium;

  const seed = bip39.mnemonicToSeedSync(mnemonic, password).slice(0, 32);
  const kp = sodium.crypto_sign_seed_keypair(seed);
  return {
    mnemonic,
    passphrase: password,
    sk: b58cencode(kp.privateKey, prefix.edsk),
    pk: b58cencode(kp.publicKey, prefix.edpk),
    pkh: b58cencode(sodium.crypto_generichash(20, kp.publicKey), prefix.tz1),
  };
}
async function sign(bytes, sk, wm) {
  await _sodium.ready;
  const sodium = _sodium;

  var bb = hex2buf(bytes);
  if (typeof wm != 'undefined') bb = mergebuf(wm, bb);
  const sig = sodium.crypto_sign_detached(
    sodium.crypto_generichash(32, bb),
    b58cdecode(sk, prefix.edsk),
    'uint8array'
  );
  const edsig = b58cencode(sig, prefix.edsig);
  const sbytes = bytes + buf2hex(sig);

  return {
    bytes,
    sig,
    edsig,
    sbytes,
  };
}

async function verify(bytes, sig, pk) {
  await _sodium.ready;
  const sodium = _sodium;

  return sodium.crypto_sign_verify_detached(
    sig,
    hex2buf(bytes),
    b58cdecode(pk, prefix.edpk)
  );
}
