const MAGIC_BYTES = require('./magic-bytes');

module.exports = {
  isBlock,
  isEndorsement,
  isGeneric,
  getMagicBytes,
  blockLevel,
};

function getMagicBytes(bytes) {
  return bytes.slice(0, 2);
}

function isBlock(bytes) {
  return getMagicBytes(bytes) === MAGIC_BYTES.block;
}

function isEndorsement(bytes) {
  return getMagicBytes(bytes) === MAGIC_BYTES.endorsement;
}

function isGeneric(bytes) {
  return getMagicBytes(bytes) === MAGIC_BYTES.generic;
}

function blockLevel(bytes) {
  if (isBlock(bytes)) {
    return parseInt(bytes.slice(10, 18), 16);
  } else {
    return parseInt(bytes.slice(bytes.length - 8), 16);
  }
}
