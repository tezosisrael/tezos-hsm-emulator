const fs = require('fs').promises;

const { generateMnemonic, generateKeys } = require('libsig');
const { isExists } = require('../file');

const dataDir = '/tmp/tezos-hub';
const filePath = `${dataDir}/data`;

// key settings
const password = undefined;
// const derivationPath = "m/44'/1729'/0'/0'";
// const scheme = 'ed25519';

/*
Creates a file in the tmp folder if doesn't exist
The file contains a mnemonic, pk, pkh and sk

TODO:
receive flags: derivation path, scheme, password, datadir
check file - if invalid/valid
*/

async function main() {
  try {
    if (!(await isExists(filePath))) {
      console.log('Creating new key');
      await createKeyAndSave();
    }
  } catch (err) {
    console.error('Failed writing to file', err);
    process.exit(1);
  }
}

async function createKeyAndSave() {
  const mnemonic = generateMnemonic();

  const data = await generateKeys(mnemonic, password); //, derivationPath, scheme);

  // const data = {
  //   mnemonic,
  //   pk: key.getPublicKey(),
  //   pkh: key.getPublicKeyHash(),
  //   sk: key.getSecretKey(),
  // };
  try {
    await fs.mkdir(dataDir);
  } catch (e) {
    console.debug('Ignoring created folder');
  }
  await fs.writeFile(filePath, JSON.stringify(data));
}

main();
