const fs = require('fs').promises;

exports.isExists = async function isExists(path) {
  try {
    await fs.access(path);
    return true;
  } catch (err) {
    return false;
  }
};
