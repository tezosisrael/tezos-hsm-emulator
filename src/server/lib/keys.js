// manages keys
const { promises: fs } = require('fs');
const { isExists } = require('../../file');

let key = null;

// load key
exports.loadKey = async function loadKey(filePath) {
  if (!(await isExists(filePath))) {
    throw new Error('Key file is missing');
  }
  const dataString = await fs.readFile(filePath);
  try {
    const keyData = JSON.parse(dataString);
    key = keyData;
    return key;
  } catch (e) {
    throw new Error('Key file is corrupted');
  }
};

// get key
exports.getKey = function getKey() {
  return key;
};
