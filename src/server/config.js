const dataDir = '/tmp/tezos-hub';
const filePath = `${dataDir}/data`;

module.exports = { dataDir, filePath };
