const express = require('express');
const logger = require('morgan');

const config = require('./config');
const { loadKey } = require('./lib/keys');
const keysRouter = require('./routes/keys');
const authorizedKeysRouter = require('./routes/authorized-keys');

loadKey(config.filePath).then((key) => console.info(`Loaded key: ${key.pkh}`));

const app = express();

app.use(logger('dev'));
app.use('/keys', keysRouter);
app.use('/authorized_keys', authorizedKeysRouter);

app.use(jsonErrorHandler);
module.exports = app;

// expressjs error handler requires 4 arguments
// eslint-disable-next-line no-unused-vars
async function jsonErrorHandler(error, req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  res.status(500).json({ id: 'failure', msg: error.message || error });
}
