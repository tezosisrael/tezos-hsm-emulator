const express = require('express');

const { getKey } = require('../../lib/keys');
const keyRoute = require('./key');
const signRoute = require('./sign');

const router = express.Router();

router.use('/:pkh', function (req, res, next) {
  const pkh = req.params.pkh;
  if (!pkh) {
    next('Missing public key hash');
  }

  const key = getKey();
  if (!key) {
    next('Missing key');
  }

  if (key.pkh != pkh) {
    next('Key is not recognized');
  }

  req.tezosKey = key;
  next();
});

router.get('/:pkh', keyRoute);

router.post('/:pkh', signRoute);

module.exports = router;
