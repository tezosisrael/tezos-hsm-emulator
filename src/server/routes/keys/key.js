module.exports = function keyRoute(req, res) {
  res.send({ public_key: req.tezosKey.pk });
};

/* 

[ { "kind": "temporary", "id": "failure",
      "msg": "no keys for the source contract manager" } ]
      */
