const { sign, isEndorsement, isBlock } = require('libsig');
const { canSign, setWatermark } = require('libsig/watermark');

const { dataDir } = require('../../config');

module.exports = async function keyRoute(req, res, next) {
  const key = req.tezosKey;
  const bytes = await parseBody(req);

  const isEndorsementOrBlock = isEndorsement(bytes) || isBlock(bytes);

  if (isEndorsementOrBlock) {
    // if ()
    // if not baking mode return error

    // else check watermark
    if (!(await canSign(dataDir, key.pkh, bytes))) {
      next('Watermark is invalid');
      return;
    }

    try {
      await setWatermark(dataDir, key.pkh, bytes);
    } catch (err) {
      console.error('failed to set watermark', err);
      next('Failed to set watermark');
      return;
    }
  }

  const response = await sign(bytes, key.sk);

  res.send({
    signature: response.edsig,
  });
};

function parseBody(req) {
  return new Promise((resolve, reject) => {
    const data = [];

    req.on('data', (chunk) => {
      data.push(chunk);
    });

    req.on('end', async () => {
      try {
        resolve(JSON.parse(data.toString('utf8')));
      } catch (err) {
        reject(err);
      }
    });
  });
}
